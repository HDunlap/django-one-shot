from django.shortcuts import render, get_object_or_404
from .models import TodoList, TodoItem
from .forms import TodoListCreate, TodoItemCreate
from django.shortcuts import redirect

# Create your views here.


def todo_list_list(request):
    todo_lists = TodoList.objects.all()

    context = {
        "todo_lists": todo_lists,
        }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)

    context = {
        "todo_list": todo_list,
        }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListCreate(request.POST)
        if form.is_valid():
            form.save()
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListCreate
    context = {
        "form": form,
        }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListCreate(request.POST, instance=post)
        if form.is_valid():
            form.save()
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListCreate(instance=post)
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemCreate(request.POST)
        if form.is_valid():
            form.save()
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemCreate
    context = {
        "form": form,
        }
    return render(request, "todos/additem.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemCreate(request.POST, instance=post)
        if form.is_valid():
            form.save()
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemCreate(instance=post)
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "todos/updateitem.html", context)
