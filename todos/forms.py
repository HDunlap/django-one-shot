from django.forms import ModelForm
from .models import TodoList, TodoItem


class TodoListCreate(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class TodoItemCreate(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
